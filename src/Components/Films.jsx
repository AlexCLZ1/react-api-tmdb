import film from '../Services/film';
import Film from './Film';
import Spinner from './Spinner';
import {useEffect, useState} from 'react';
import DataTable from 'react-data-table-component';
import ExpandableItem from './ExpandableItem';
import { useNavigate } from "react-router-dom";

const Films = () => {

    const [films, setFilms] = useState([]);
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);
    const [totalItems, setTotalItems] = useState(0);
    const navigate= useNavigate();

    async function fetchFilm() {
        try {
            let response = await film.getFilm();
            setFilms(response.data.results);
            setLoading(false);
            setTotalItems(response.data.total_results);
        } catch (error) {
            console.log(error)

        }
    }

    useEffect( () => {

    })

    useEffect( () => {
        fetchFilm().then(r => '');
    }, [page])

    const columns = [
        {
            name: "Titre",
            selector: row => row.title
        },
        {
            name: "Date de sortie",
            selector: row => row.release_date
        }
    ]

    const handleChangePage = (page) => {
      setPage(page);
        }

    return <>
        {/* {loading === true ?
        <Spinner /> 
        : 
        films.map((film, i) => {
            return <>
                <Film film={film}/>
            </>
        })} */}

        <div style = {{width:"80%", marginLeft:"1rem", marginTop:"1rem"}}>
            <DataTable 
            data={films} 
            columns={columns}
            expandableRows={true}
            expandableRowsComponent={ExpandableItem}
            expandOnRowClicked={true}
            expandableRowsHideExpander={true}
            pagination={true}
            paginationServer={true}
            paginationPerPage={20}
            paginationTotalRows={500*20}
            onChangePage={handleChangePage}
            onRowDoubleClicked={row => navigate("/film/"+row.id)}
            />
        </div>
    </>
}

export default Films;