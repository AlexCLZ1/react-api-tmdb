import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

const Header = () => {
    
    return (
        <>
        <Navbar bg="primary" variant="primary">
          <Container>
            <Navbar.Brand href="http://localhost:3000" >
              <img
                alt=""
                src="https://play-lh.googleusercontent.com/FeRWKSHpYNEW21xZCQ-Y4AkKAaKVqLIy__PxmiE_kGN1uRh7eiB87ZFlp3j1DRp9r8k"
                width="50"
                height="50"
                className="d-inline-block align-top"
              />{' '}
             
            </Navbar.Brand>
            <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="http://localhost:3000" className='text-light'>Accueil</Nav.Link>
            <Nav.Link href="https://www.google.com/search?q=hitler&client=opera-gx&hs=cGl&sxsrf=AJOqlzWElaD3lJiVQOlfvhRZVZRS6s1ZWQ:1677835986734&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiCwOuPur_9AhXESaQEHRPmAbsQ_AUoAXoECAEQAw&biw=1503&bih=782&dpr=1.25#imgrc=zCNutSOsDrOOiM" className='text-light'>Champions</Nav.Link>
            <Nav.Link href="#home" className='text-light'>Game Mode</Nav.Link>
            <Nav.Link href="#link" className='text-light'>Statistiques</Nav.Link>
            <Nav.Link href="#home" className='text-light'>Classements</Nav.Link>
            <Nav.Link href="#link" className='text-light'>Pro Matches</Nav.Link>
            <Nav.Link href="#home" className='text-light'>Recherche Multiple</Nav.Link>
            </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </>
    );
  }

export default Header;