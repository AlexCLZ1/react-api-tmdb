import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import FilmApi from "../Services/film";
import {URL_IMAGE} from '../Services/url';




const FilmDetails = () => {
    const { id } = useParams();
    const [film, setFilm] = useState({});

    const fetchFilmByID = async () => {
        try {
            let response = await FilmApi.getFilmById(id);
            setFilm(response.data)
        } catch (e) {
            console.log(e);
        }
    }


    useEffect(() => {
        fetchFilmByID().then(r=>"");
    }, [])


    return (
        <div>
            <div style={{textAlign:"center", marginTop:"2rem"}}>
                <img src={URL_IMAGE+film.poster_path} alt="" width={"30%"}/>

            </div>
            <div>
                <h2 style={{textAlign:"center"}}>{film.title}</h2>
            </div>
            </div>
    );
}



export default FilmDetails;