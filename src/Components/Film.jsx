import {useEffect} from "react";
import {URL_IMAGE} from '../Services/url';
import { Link } from 'react-router-dom';


const Film = ({film}) => {

    useEffect(() => {
        console.log(film)
    }, [film])

    return <>

            <div className="card">
               <img src={URL_IMAGE + film.poster_path} />
                <div className="card-content">
                    <div className="card-title">{film.original_title}</div>
                    <p>{film.overview}</p>
                </div>
                <div class="center-btn">
                    <Link to={'/film/'+film.id}>
                    <a href="#" className="card-btn">Voir plus</a>
                    </Link>
                </div>
            </div>

      
    
  

    </>
}

// {URL_IMAGE+film.poster_path}

export default Film;