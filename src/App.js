import './App.css';
import { BrowserRouter, Routes, Route} from "react-router-dom";
import Accueil from './Pages/Accueil.jsx';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Components/Header.jsx';
import {useEffect} from "react";
import auth from "./Services/auth";
import FilmDetails from "./Components/FilmDetails.jsx";

function App() {


  useEffect( () => { 
    auth.setAxiosToken();
  }, [])


  return (
    <div>
      {/* On appelle les noirs navbar */}
       <Header />
       <div className="container">
        <div className="cards-container">
       {/* Appel des routes */}
        <BrowserRouter>
        <Routes>
          <Route element={ <Accueil />  } path={"/"} />
          <Route element={ <FilmDetails />  } path={"/film/:id"} />
        </Routes>
        

        </BrowserRouter>
        </div>
        </div>
        </div>
  );
}

export default App;
