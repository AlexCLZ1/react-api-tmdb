import axios from 'axios';

function setAxiosToken(){
    axios.defaults.headers['Authorization'] = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhYTc2ZGJkM2UyYTg0NzJkNjU4NjU0YzQ2ZWU1NGNiNiIsInN1YiI6IjY0MDBhMGVjMjc4ZDhhMDA3YTEyOWMyNyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.uH0XBbmB0T5n5EvIqU3OSWJOmsQLZqogsJ3lSCrZ9Dw";
}

export default {
    setAxiosToken,
}