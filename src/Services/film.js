import axios from "axios";
import {URL} from "./url";

function getFilm(page){
    return axios.get(URL+'discover/movie?language=fr-FR&page='+page);
}

function getFilmById(id){
    return axios.get(URL+'movie/'+id+'?language=fr')
}

export default {
    getFilm, getFilmById
}

